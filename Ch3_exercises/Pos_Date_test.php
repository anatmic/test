<?php
error_reporting(-1);
ini_set('display_errors', 1);

require_once '../bootstrap.php';

try {
    $local = new Pos_Date();
    echo '<p>Time now: ' . $local->format('F jS, Y h:i A') .  '</p>';
    $local->setTime(12, 30);
    $local->setDate(2008, 88, 8);
    echo '<p>Date and time reset: ' . $local->format('F jS, Y h:i A') . '</p>';
} catch (Exception $e) {
    echo $e;
}